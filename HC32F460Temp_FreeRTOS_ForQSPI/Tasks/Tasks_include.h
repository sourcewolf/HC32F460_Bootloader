#ifndef TASKS_INC_H
#define TASKS_INC_H
#include "../Tasks/Task_LED.h"
#include "../Tasks/Task_display.h"
#include "../Tasks/Task_Key.h"
#include "../Tasks/Task_FileSystem.h"
#include "../Tasks/Task_ADC.h"
#include "../Tasks/Task_USB.h"
#include "Hw_I2C.h"
#include "Hw_I2C_Slave.h"
#endif

