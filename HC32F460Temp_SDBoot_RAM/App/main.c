#include "Main_include.h"
uint16_t ResetFlag;
FATFS FatFs;
FRESULT fr;
FIL Myfile;
UINT bw,br;
FILINFO f_info;
#define  APP_START_ADDRESS  0x1FFF8000
#define SECTORSIZE  8192 
#define SECTORSIZE_W	2048
typedef union EEFLASH{
    uint32_t U32_Data[SECTORSIZE_W];
    char U8_Data[SECTORSIZE];
}EEFLASH_data_t;
char line[82] = "abcdefghijklmnopqrstuvwxyz";
EEFLASH_data_t Readbuffer;
stc_efm_unique_id_t UID_data;
en_result_t FlashWritePage(uint32_t u32Addr, EEFLASH_data_t *eeflashdata,uint32_t len)
{
    uint32_t i,pageaddr;
    pageaddr = u32Addr&0xFFFFE000;//取页起始地址，以保证数据从起始地址写
		
   /* Unlock EFM. */ 
    EFM_Unlock();
    
    /* Enable flash. */
    EFM_FlashCmd(Enable);
    /* Wait flash ready. */
    while(Set != EFM_GetFlagStatus(EFM_FLAG_RDY));
		
    EFM_SectorErase(pageaddr);//擦除页
		
    pageaddr = u32Addr;    
    for(i = 0; i < len;i++)
    {
        EFM_SingleProgram(pageaddr,eeflashdata->U32_Data[i]);
        pageaddr += 4;
    }		
    EFM_Lock();
}
static void GotoApp(void)
{
//	stc_qspi_comm_protocol_t stcQspiCommProtocol;
//	MEM_ZERO_STRUCT(stcQspiCommProtocol);
//	stcQspiCommProtocol.enReadMode = QspiReadModeFourWiresOutput;
//    QSPI_CommProtocolConfig(&stcQspiCommProtocol);
//	QSPI_XipModeCmd(0xFF,Enable);
//	EFM_Unlock();
//	EFM_SetLatency(EFM_LATENCY_15);
//	EFM_Lock();
	uint32_t app_start_address;
	/* Load the Reset Handler address of the application */
	app_start_address = *(uint32_t *)(APP_START_ADDRESS + 4);

	/**
	 * Test reset vector of application @APP_START_ADDRESS+4
	 * Stay in SAM-BA if *(APP_START+0x4) == 0xFFFFFFFF
	 * Application erased condition
	 */
	if (app_start_address == 0xFFFFFFFF) {
		return;// 1;
	}
	/* Rebase the Stack Pointer */
	__set_MSP(*(uint32_t *) APP_START_ADDRESS);

	/* Rebase the vector table base address */
	SCB->VTOR = ((uint32_t) APP_START_ADDRESS & SCB_VTOR_TBLOFF_Msk);

	/* Jump to application Reset Handler in the application */
    (*((void(*)(void))app_start_address))();
	return;// 0;
}
extern void W25QXX_Write_Page(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void Inter_RAM_Write(uint32_t WriteAddr, uint8_t* pBuffer, uint16_t NumByteToWrite)
{
	uint32_t i;
	uint8_t *pram = (uint8_t*)WriteAddr;
	for(i= 0;i<NumByteToWrite;i++)
	{
		pram[i]= pBuffer[i];
	}
}
int main(void)
{
#if 0		
	uint32_t addr =  0x40000;
	uint8_t status = 0;
    FATFS fs;
    FRESULT res;
    ResetFlag = M4_SYSREG->RMU_RSTF0;
    M4_SYSREG->RMU_RSTF0_f.CLRF = 1;   
    system_clk_init();
	User_Gpio_Init();
    Ddl_UartInit();
	UID_data = EFM_ReadUID();
	printf("UID1 =  %x\r\n",UID_data.uniqueID1);
	printf("UID2 =  %x\r\n",UID_data.uniqueID2);
	printf("UID3 =  %x\r\n",UID_data.uniqueID3);
	hd_sdio_hw_init();
	disk_initialize(SD_Card);    
	for(;;)
	{
		switch(status)
		{
			case 0:
				fr = f_mount(SD_Card,&FatFs);//驱动器0
				if(fr == FR_OK)
				{
					printf("Disk init\r\n");
					PORT_SetBits(LED1_PORT,LED1_Pin);
					PORT_ResetBits(LED0_PORT,LED0_Pin);
					status = 1;
				}
				else
				{
					printf("Error ------ None disk\r\n");
					PORT_SetBits(LED0_PORT,LED0_Pin);
					PORT_ResetBits(LED1_PORT,LED1_Pin);
					status = 4;
					GotoApp();
				}
				break;
			case 1:
				fr = f_open(&Myfile,"Firmware.bin",FA_READ);
				if(fr==FR_OK)
				{
					printf("Find the firmware file we will continue to update with it!\r\n");
					PORT_SetBits(LED1_PORT,LED1_Pin);
					PORT_ResetBits(LED0_PORT,LED0_Pin);
					status = 2;
				}
				else
				{
					printf("Error ------ No such file name -- Firmware.bin\r\n");
					PORT_SetBits(LED0_PORT,LED0_Pin);
					PORT_ResetBits(LED1_PORT,LED1_Pin);
					status = 4;
					GotoApp();
				}
				break;
			case 2:
				flag_key0 = 0;
				printf("please press key0 to continue...\r\n");
				while(flag_key0 == 0);
				flag_key0 = 0;
				status = 3;
				break;
			case 3:
				f_lseek(&Myfile,0);
				for(;;)
				{
					fr = f_read(&Myfile,Readbuffer.U8_Data,SECTORSIZE,&br);
					if(br != 0)
					{
						printf("Update firmware at address %x...\r\n",addr);
						FlashWritePage(addr,&Readbuffer,br);
						addr+=0x2000;
						PORT_Toggle(LED0_PORT,LED0_Pin);
					}
					else
					{
						PORT_SetBits(LED1_PORT,LED1_Pin);
						PORT_ResetBits(LED0_PORT,LED0_Pin);
						break;
					}
				}
				fr = f_close(&Myfile);
				printf("Firmware update success!\r\n");
//				printf("remove firmware.bin\r\n");
//				fr = f_unlink("Firmware.bin");
				status = 4;
				break;
			case 4:
				flag_key0 = 0;
				printf("please press key0 to run App...\r\n");
				while(flag_key0 == 0);
				flag_key0 = 0;	
				status = 5;
			default:
				break;
		}
		if(status >=5)
		{
			break;
		}
	}
#else
	uint32_t addr =  0;
	uint8_t status = 0;
    FATFS fs;
    FRESULT res;
	stc_qspi_comm_protocol_t stcQspiCommProtocol;
	MEM_ZERO_STRUCT(stcQspiCommProtocol);
    ResetFlag = M4_SYSREG->RMU_RSTF0;
    M4_SYSREG->RMU_RSTF0_f.CLRF = 1;   
    system_clk_init();
	User_Gpio_Init();
    Ddl_UartInit();
	UID_data = EFM_ReadUID();
	printf("UID1 =  %x\r\n",UID_data.uniqueID1);
	printf("UID2 =  %x\r\n",UID_data.uniqueID2);
	printf("UID3 =  %x\r\n",UID_data.uniqueID3);
	QspiFlash_Init();
	stcQspiCommProtocol.enReadMode = QspiReadModeFourWiresOutput;
    QSPI_CommProtocolConfig(&stcQspiCommProtocol);
	hd_sdio_hw_init();
	disk_initialize(SD_Card); 
	for(;;)
	{
		switch(status)
		{
			case 0:
				fr = f_mount(SD_Card,&FatFs);//驱动器0
				if(fr == FR_OK)
				{
					printf("Disk init\r\n");
					PORT_SetBits(LED1_PORT,LED1_Pin);
					PORT_ResetBits(LED0_PORT,LED0_Pin);
					status = 1;
				}
				else
				{
					printf("Error ------ None disk\r\n");
					PORT_SetBits(LED0_PORT,LED0_Pin);
					PORT_ResetBits(LED1_PORT,LED1_Pin);
					status = 4;
					EFM_Unlock();
					EFM_SetLatency(EFM_LATENCY_15);
					EFM_Lock();
					GotoApp();
				}
				break;
			case 1:
				fr = f_open(&Myfile,"Firmware.bin",FA_READ);
				if(fr==FR_OK)
				{
					printf("Find the firmware file we will continue to update with it!\r\n");
					PORT_SetBits(LED1_PORT,LED1_Pin);
					PORT_ResetBits(LED0_PORT,LED0_Pin);
					status = 2;
				}
				else
				{
					printf("Error ------ No such file name -- Firmware.bin\r\n");
					PORT_SetBits(LED0_PORT,LED0_Pin);
					PORT_ResetBits(LED1_PORT,LED1_Pin);
					status = 4;
					EFM_Unlock();
					EFM_SetLatency(EFM_LATENCY_15);
					EFM_Lock();
					GotoApp();
				}
				break;
			case 2:
				flag_key0 = 0;
				printf("please press key0 to continue...\r\n");
				while(flag_key0 == 0);
				flag_key0 = 0;
				status = 3;
				break;
			case 3:
				f_lseek(&Myfile,0);
				stcQspiCommProtocol.enReadMode = QspiReadModeStandard;
				QSPI_CommProtocolConfig(&stcQspiCommProtocol);
				for(;;)
				{
					fr = f_read(&Myfile,Readbuffer.U8_Data,0x100,&br);
					if(br != 0)
					{
						printf("Update firmware at address %x...\r\n",addr+APP_START_ADDRESS);
//						FlashWritePage(addr,&Readbuffer,br);		
//						if((addr&0xFFF)==0)
//						{
//							QspiFlash_Erase4KbSector(addr);
//						}
						Inter_RAM_Write(addr+APP_START_ADDRESS,(uint8_t *)Readbuffer.U8_Data,br);
						addr+=0x100;
						PORT_Toggle(LED0_PORT,LED0_Pin);
					}
					else
					{
						PORT_SetBits(LED1_PORT,LED1_Pin);
						PORT_ResetBits(LED0_PORT,LED0_Pin);
						break;
					}
				}
				fr = f_close(&Myfile);
				printf("Firmware update success!\r\n");
//				printf("remove firmware.bin\r\n");
//				fr = f_unlink("Firmware.bin");
				status = 4;
				break;
			case 4:
				flag_key0 = 0;
				printf("please press key0 to run App...\r\n");
				while(flag_key0 == 0);
				flag_key0 = 0;	
				status = 5;
			default:
				break;
		}
		if(status >=5)
		{
			break;
		}
	}
#endif
	
	GotoApp();
    while(1)
    {
		
    }
}
