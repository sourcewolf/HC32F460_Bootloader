#ifndef QSPI_FLASH_H
#define QSPI_FLASH_H
#include "hc32_ddl.h"
/* LED0 Port/Pin definition */
#define LED0_PORT                       (PortE)
#define LED0_PIN                        (Pin06)

#define LED0_ON()                       (PORT_SetBits(LED0_PORT, LED0_PIN))
#define LED0_OFF()                      (PORT_ResetBits(LED0_PORT, LED0_PIN))
#define LED0_TOGGLE()                   (PORT_Toggle(LED0_PORT, LED0_PIN))

/* LED1 Port/Pin definition */
#define LED1_PORT                       (PortA)
#define LED1_PIN                        (Pin07)

#define LED1_ON()                       (PORT_SetBits(LED1_PORT, LED1_PIN))
#define LED1_OFF()                      (PORT_ResetBits(LED1_PORT, LED1_PIN))
#define LED1_TOGGLE()                   (PORT_Toggle(LED1_PORT, LED1_PIN))

/* KEY0 Port/Pin definition */
#define KEY0_PORT                       (PortD)
#define KEY0_PIN                        (Pin03)

/* QSPCK Port/Pin definition */
#define QSPCK_PORT                      (PortC)
#define QSPCK_PIN                       (Pin06)

/* QSNSS Port/Pin definition */
#define QSNSS_PORT                      (PortC)
#define QSNSS_PIN                       (Pin07)

/* QSIO0 Port/Pin definition */
#define QSIO0_PORT                      (PortD)
#define QSIO0_PIN                       (Pin08)

/* QSIO1 Port/Pin definition */
#define QSIO1_PORT                      (PortD)
#define QSIO1_PIN                       (Pin09)

/* QSIO2 Port/Pin definition */
#define QSIO2_PORT                      (PortD)
#define QSIO2_PIN                       (Pin10)

/* QSIO3 Port/Pin definition */
#define QSIO3_PORT                      (PortD)
#define QSIO3_PIN                       (Pin11)

/* QSPI memory bus address definition */
#define QSPI_BUS_ADDRESS                (0x98000000ul)
/* FLASH parameters definition */
#define FLASH_PAGE_SIZE                 (0x100u)
#define FLASH_SRCTOR_SIZE               (0x1000u)
#define FALSH_MAX_ADDR                  (0x800000ul)
#define FLASH_DUMMY_BYTE_VALUE          (0xffu)
#define FLASH_BUSY_BIT_MASK             (0x01u)

/* FLASH instruction definition */
#define FLASH_INSTR_WRITE_ENABLE        (0x06u)
#define FLASH_INSTR_PAGE_PROGRAM        (0x02u)
#define FLASH_INSTR_ERASE_4KB_SECTOR    (0x20u)
#define FLASH_INSTR_ERASE_CHIP          (0xC7u)
#define FLASH_INSTR_READ_SR1            (0x05u)
#define FLASH_INSTR_READ_SR2            (0x35u)
#define FLASH_INSTR_READ_SR3            (0x15u)

void QspiFlash_Init(void);
void QspiFlash_WriteEnable(void);
en_result_t QspiFlash_WaitForWriteEnd(void);
en_result_t QspiFlash_WritePage(uint32_t u32Addr, const uint8_t pData[], uint16_t len);
en_result_t QspiFlash_Erase4KbSector(uint32_t u32Addr);
void QspiFlash_EraseChip(void);
uint8_t QspiFlash_ReadStatusRegister(uint8_t u8Reg);
#endif


