#include "hc32_ddl.h"
#include "System_InterruptCFG_Def.h"
unsigned char SPI_DATA;
/* Choose SPI master or slave mode */
#define SPI_MASTER_MODE
//#define SPI_SLAVE_MODE
/* SPI_SCK Port/Pin definition */
#define SPI_SCK_PORT                    PortB
#define SPI_SCK_PIN                     Pin12
#define SPI_SCK_FUNC                    Func_Spi3_Sck

/* SPI_NSS Port/Pin definition */
#define SPI_NSS_PORT                    PortB
#define SPI_NSS_PIN                     Pin13
#define SPI_NSS_FUNC                    Func_Spi3_Nss0

/* SPI_MOSI Port/Pin definition */
#define SPI_MOSI_PORT                   PortB
#define SPI_MOSI_PIN                    Pin14
#define SPI_MOSI_FUNC                   Func_Spi3_Mosi

/* SPI_MISO Port/Pin definition */
#define SPI_MISO_PORT                   PortB
#define SPI_MISO_PIN                    Pin15
#define SPI_MISO_FUNC                   Func_Spi3_Miso

/* SPI unit and clock definition */
#define SPI_UNIT                        M4_SPI3
#define SPI_UNIT_CLOCK                  PWC_FCG1_PERIPH_SPI3
#define SPI_TX_INT_SOURCE               INT_SPI3_SRTI
#define SPI_RX_INT_SOURCE               INT_SPI3_SRRI
#define SPI_ERR_INT_SOURCE              INT_SPI3_SPEI
#define SPI_ERR_IDEL_SOURCE             INT_SPI3_SPII

#define SPI_DMA_UNIT2                (M4_DMA2)
#define SPI_DMA_CH                  (DmaCh0)
#define SPI_DMA_CLK                 PWC_FCG0_PERIPH_DMA2
#define SPI_DMA_TRNCNT              (50u)//�������
#define DMA_BLKSIZE             (1u)
#define SPI_DMA_RPT_SIZE            (50u)
#define DMA_INT_SRC             INT_DMA2_BTC0
#define DMA_Trg_Src             EVT_SPI3_SRRI

uint8_t SPI1_RX_Data;
bool flag_SPI3_RX, flag_SPI3_TX;
uint8_t RXbuff[SPI_DMA_TRNCNT],counter;
void USER_SPI3_TX_Callback(void)
{  
    SPI_SendData8(SPI_UNIT,0xFF);
    flag_SPI3_TX = true;
}
void USER_SPI3_RX_Callback(void)
{   
//    SPI_SetReadDataRegObject(SPI_UNIT,SpiReadReceiverBuffer);
    SPI1_RX_Data = SPI_ReceiveData8(SPI_UNIT);
    if(SPI1_RX_Data == 0x55 && counter > 1)
    {
        if(RXbuff[1] != 0x22)
        {
            counter = 0;
        }       
    }
    RXbuff[counter++] = SPI1_RX_Data;
    if(counter>=50)
    {
        counter = 0;
    }
    flag_SPI3_RX = true;
}
void USER_SPI3_IDEL_Callback(void)
{
//    SPI_UNIT->SR_f.IDLNF = 1;
    counter = 0;;
}
void USER_SPI3_ERR_Callback(void)
{
    if(SPI_UNIT->SR_f.MODFERF)//ģʽ����
    {
        SPI_UNIT->SR_f.MODFERF = 0;
    }
    if(SPI_UNIT->SR_f.UDRERF)//Ƿ�ش���
    {
        SPI_UNIT->SR_f.UDRERF = 0;
    }
    if(SPI_UNIT->SR_f.PERF)//��żУ�����
    {
        SPI_UNIT->SR_f.PERF = 0;
    }
    if(SPI_UNIT->SR_f.OVRERF)//���ش���
    {
        SPI_UNIT->SR_f.OVRERF = 0;
    }
    SPI_UNIT->CR1_f.SPE = 1;
}
void User_SPI_Init(void)
{
    stc_spi_init_t stcSpiInit;
    stc_irq_regi_conf_t stcIrqRegiConf;

    /* configuration structure initialization */
    MEM_ZERO_STRUCT(stcSpiInit);
    MEM_ZERO_STRUCT(stcIrqRegiConf);

    /* Configuration peripheral clock */
    PWC_Fcg1PeriphClockCmd(SPI_UNIT_CLOCK, Enable);

    /* Configuration SPI pin */
    PORT_SetFunc(SPI_SCK_PORT, SPI_SCK_PIN, SPI_SCK_FUNC, Disable);
    PORT_SetFunc(SPI_NSS_PORT, SPI_NSS_PIN, SPI_NSS_FUNC, Disable);
    PORT_SetFunc(SPI_MOSI_PORT, SPI_MOSI_PIN, SPI_MOSI_FUNC, Disable);
    PORT_SetFunc(SPI_MISO_PORT, SPI_MISO_PIN, SPI_MISO_FUNC, Disable);

    /* Configuration SPI structure */
    stcSpiInit.enClkDiv = SpiClkDiv128;
    stcSpiInit.enFrameNumber = SpiFrameNumber1;
    stcSpiInit.enDataLength = SpiDataLengthBit8;
    stcSpiInit.enFirstBitPosition = SpiFirstBitPositionMSB;
    stcSpiInit.enSckPolarity = SpiSckIdleLevelLow;
    stcSpiInit.enSckPhase = SpiSckOddSampleEvenChange;
    stcSpiInit.enReadBufferObject = SpiReadReceiverBuffer;
    stcSpiInit.enWorkMode = SpiWorkMode4Line;
    stcSpiInit.enTransMode = SpiTransFullDuplex;
    stcSpiInit.enCommAutoSuspendEn = Disable;
    stcSpiInit.enModeFaultErrorDetectEn = Disable;
    stcSpiInit.enParitySelfDetectEn = Disable;
    stcSpiInit.enParityEn = Disable;
    stcSpiInit.enParity = SpiParityEven;

#ifdef SPI_MASTER_MODE
    stcSpiInit.enMasterSlaveMode = SpiModeMaster;
    stcSpiInit.stcDelayConfig.enSsSetupDelayOption = SpiSsSetupDelayCustomValue;
    stcSpiInit.stcDelayConfig.enSsSetupDelayTime = SpiSsSetupDelaySck1;
    stcSpiInit.stcDelayConfig.enSsHoldDelayOption = SpiSsHoldDelayCustomValue;
    stcSpiInit.stcDelayConfig.enSsHoldDelayTime = SpiSsHoldDelaySck1;
    stcSpiInit.stcDelayConfig.enSsIntervalTimeOption = SpiSsIntervalCustomValue;
    stcSpiInit.stcDelayConfig.enSsIntervalTime = SpiSsIntervalSck6PlusPck2;
    stcSpiInit.stcSsConfig.enSsValidBit = SpiSsValidChannel0;
    stcSpiInit.stcSsConfig.enSs0Polarity = SpiSsLowValid;
#endif

#ifdef SPI_SLAVE_MODE
    stcSpiInit.enMasterSlaveMode = SpiModeSlave;
    stcSpiInit.stcSsConfig.enSsValidBit = SpiSsValidChannel0;
    stcSpiInit.stcSsConfig.enSs0Polarity = SpiSsLowValid;
#endif
    SPI_Init(SPI_UNIT, &stcSpiInit);

    /* SPI3 tx interrupt */
    stcIrqRegiConf.enIntSrc = SPI_TX_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI3_TX_IRQn;
    stcIrqRegiConf.pfnCallback = USER_SPI3_TX_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);
    /* Enable software trigger interrupt */
//    enIntEnable(Int5);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
//    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);

    /* SPI3 rx interrupt */
    stcIrqRegiConf.enIntSrc = SPI_RX_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI3_RX_IRQn;
    stcIrqRegiConf.pfnCallback = USER_SPI3_RX_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
//    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* SPI3 Error interrupt */
    stcIrqRegiConf.enIntSrc = SPI_ERR_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI3_ERR_IRQn;
    stcIrqRegiConf.pfnCallback = USER_SPI3_ERR_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* SPI3 idel interrupt */
    stcIrqRegiConf.enIntSrc = SPI_ERR_IDEL_SOURCE;
    stcIrqRegiConf.enIRQn = SPI3_IDEL_IRQn;
    stcIrqRegiConf.pfnCallback = USER_SPI3_IDEL_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* Enable SPI */
    SPI_Cmd(SPI_UNIT, Enable);
//    SPI_UNIT->CR1_f.SPE = 1;
    Ddl_Delay1ms(10);
    SPI_IrqCmd(SPI_UNIT, SpiIrqReceive, Enable);
    SPI_IrqCmd(SPI_UNIT, SpiIrqSend, Enable);
    SPI_IrqCmd(SPI_UNIT, SpiIrqError, Enable);
//    SPI_IrqCmd(SPI_UNIT, SpiIrqIdel, Enable);
//    SPI_Cmd(SPI_UNIT, Enable);
    
}

void USER_SPI_TEST(void)
{	
    while(SPI_UNIT->SR_f.TDEF == 0);
    SPI_SendData8(SPI_UNIT,0x55);	
}
void SPI_DMA_Callback(void)
{
    DMA_ClearIrqFlag(SPI_DMA_UNIT2,SPI_DMA_CH, TrnCpltIrq);
    DMA_SetTransferCnt(SPI_DMA_UNIT2,SPI_DMA_CH,SPI_DMA_TRNCNT);
    DMA_SetDesAddress(SPI_DMA_UNIT2,SPI_DMA_CH,(uint32_t)(RXbuff));
    DMA_ChannelCmd(SPI_DMA_UNIT2, SPI_DMA_CH,Enable); 
}
void SPI_DMA_Init(void)
{
    stc_dma_config_t stcDmaCfg;
    stc_irq_regi_conf_t stcIrqRegiConf;
    MEM_ZERO_STRUCT(stcDmaCfg);
    MEM_ZERO_STRUCT(stcIrqRegiConf);
    
    stcDmaCfg.u16BlockSize = DMA_BLKSIZE;//
    stcDmaCfg.u16TransferCnt = SPI_DMA_TRNCNT;//
    
    stcDmaCfg.u32DesAddr = (uint32_t)(&RXbuff[0]);//(&DMA0_Dre_Data[0]);//Target Address
    stcDmaCfg.u32SrcAddr = (uint32_t)(&(SPI_UNIT->DR));//USART2_DR_ADDRESS;//(uint32_t)(&DMA0_Src_data[0]);//Source Address
    
    /* Set repeat size. */
    stcDmaCfg.u16SrcRptSize = 1;
    stcDmaCfg.u16DesRptSize = SPI_DMA_RPT_SIZE;

    /* Disable linked list transfer. */
    stcDmaCfg.stcDmaChCfg.enLlpEn = Disable;     
    /* Enable repeat function. */
    stcDmaCfg.stcDmaChCfg.enSrcRptEn = Enable;
    stcDmaCfg.stcDmaChCfg.enDesRptEn = Enable;   
    /* Set source & destination address mode. */
    stcDmaCfg.stcDmaChCfg.enSrcInc = AddressFix;//��ַ����
    stcDmaCfg.stcDmaChCfg.enDesInc = AddressIncrease;
    /* Enable interrup. */
    stcDmaCfg.stcDmaChCfg.enIntEn = Enable;
    /* Set data width 32bit. */
    stcDmaCfg.stcDmaChCfg.enTrnWidth = Dma8Bit;

//    M4_MSTP->FCG0PC = 0xA5A50001;
//    M4_MSTP->FCG0_f.DMA1 = Reset;
//    M4_MSTP->FCG0PC = 0xA5A50000;
    PWC_Fcg0PeriphClockCmd(SPI_DMA_CLK, Enable);
   
    /* Enable DMA1. */
    DMA_Cmd(SPI_DMA_UNIT2,Enable);   
    /* Initialize DMA. */
    DMA_InitChannel(SPI_DMA_UNIT2, SPI_DMA_CH, &stcDmaCfg);
    /* Enable DMA1 channel0. */
    DMA_ChannelCmd(SPI_DMA_UNIT2, SPI_DMA_CH,Enable);
    /* Clear DMA transfer complete interrupt flag. */
    DMA_ClearIrqFlag(SPI_DMA_UNIT2, SPI_DMA_CH,TrnCpltIrq);
    
    stcIrqRegiConf.enIntSrc = INT_DMA2_TC0;
    stcIrqRegiConf.enIRQn = DMA2_CH0_IRQn;
    stcIrqRegiConf.pfnCallback =  SPI_DMA_Callback;   
    
    enIrqRegistration(&stcIrqRegiConf);
	NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);//Enable Interrupt

    
    /* Enable PTDIS(AOS) clock*/
    PWC_Fcg0PeriphClockCmd(PWC_FCG0_PERIPH_PTDIS,Enable);
    
    DMA_SetTriggerSrc(SPI_DMA_UNIT2,SPI_DMA_CH,DMA_Trg_Src);
       
//    M4_AOS->INT_SFTTRG_f.STRG = 1;
//    
//    while(Set != DMA_GetIrqFlag(DMA_UNIT,DMA_CH, TrnCpltIrq))
//    {
//        M4_AOS->INT_SFTTRG_f.STRG = 1;
//    }
}
