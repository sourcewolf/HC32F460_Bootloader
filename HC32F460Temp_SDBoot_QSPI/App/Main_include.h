#ifndef MAIN_INCLUDE_H
#define MAIN_INCLUDE_H
#include "hc32_ddl.h"
#include "system_Clk.h"
#include "hd_sdio.h"
#include "ff.h"
#include "diskio.h"
#include "System_PowerDown.h"
#include "user_Gpio.h"
#include "QSPI_Flash.h"
#endif
