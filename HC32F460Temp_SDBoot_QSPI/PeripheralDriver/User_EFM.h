#ifndef USER_EFM_H
#define USER_EFM_H
#include "hc32_ddl.h"

#define SECTORSIZE  8192 
#define SECTORSIZE_W	2048
#define OTP_SECTORSIZE  64
#define OTP_SECTORSIZE_W 16

#define FLASH_SECTOR62_ADRR             0x0007C000
typedef struct
{
    union
    {
        uint8_t Data_8bit[64];
        uint16_t Data_16bit[32];
    };    
}CRC_Data_16bit_t;
typedef struct
{
    union
    {
        uint8_t Data_8bit[128];
        uint32_t Data_16bit[32];
    };    
}CRC_Data_32bit_t;   


typedef union EEFLASH{
    uint32_t U32_Data[SECTORSIZE_W];
    uint8_t U8_Data[SECTORSIZE];
}EEFLASH_data_t;

typedef union OTPData{
    uint32_t U32_data[OTP_SECTORSIZE_W];
    uint8_t U8_data[OTP_SECTORSIZE];
}OTPData_t;
extern EEFLASH_data_t Buffer;
en_result_t FlashWritePage(uint32_t u32Addr, EEFLASH_data_t *eeflashdata);
en_result_t OTP_Write_Data(uint32_t u32Addr, uint32_t *data, uint8_t len);
en_result_t OTP_Verify_Data(uint32_t u32Addr, uint32_t *data, uint8_t len);
void testOTP(void);
void User_Test_CRC(void);
#endif

