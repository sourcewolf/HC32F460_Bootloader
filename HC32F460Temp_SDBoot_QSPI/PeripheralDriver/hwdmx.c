///////////////////////////////////////////////////////////////////////////////////////////////////
//公司名称：宏远电子 
//模 块 名	hwuart.c
//创 建 者：Ace
//创建日期：2013/03/20
//功能描述： 
//其他说明： 
//修 改 者： 
//修改说明： 
//修改日期：2013/03/20
//版    本：V1.0
/////////////////////////////////////////////////////////////////////////////////////////////////
#include "mod_port.h"
#include "hwdmx.h"                  

u1_t hwdmx_rcvBuf[DMX_UART_RECEIVE_BUF]; 
u1_t hwdmx_dataIsReady;
static  u2_t _receiveIndex;

#define USART_CH                        M4_USART3

#define USART_BAUDRATE                  (115200)

#define USART_RX_PORT                   PortE
#define USART_RX_PIN                    Pin02
#define USART_RX_FUNC                   Func_Usart3_Rx

#define USART_TX_PORT                   PortE
#define USART_TX_PIN                    Pin03
#define USART_TX_FUNC                   Func_Usart3_Tx

#define USART_RI_NUM                    INT_USART3_RI
#define USART_EI_NUM                    INT_USART3_EI
#define USART_TI_NUM                    INT_USART3_TI
#define USART_TCI_NUM                   INT_USART3_TCI

static void UsartRxIrqCallback(void);

static void UsartErrIrqCallback(void)
{
	if (Set == USART_GetStatus(USART_CH, UsartFrameErr))
	{
			USART_ClearStatus(USART_CH, UsartFrameErr);
	}

	if (Set == USART_GetStatus(USART_CH, UsartParityErr))
	{
			USART_ClearStatus(USART_CH, UsartParityErr);
	}

	if (Set == USART_GetStatus(USART_CH, UsartOverrunErr))
	{
			USART_ClearStatus(USART_CH, UsartOverrunErr);
	}
}

void hwdmx_init(void)
{
	stc_usart_uart_init_t stcUsartConf;
	stc_irq_regi_conf_t stcIrqRegiCfg;
  stc_port_init_t Port_CFG;
	
	MEM_ZERO_STRUCT(stcUsartConf);
	MEM_ZERO_STRUCT(stcIrqRegiCfg);
	MEM_ZERO_STRUCT(Port_CFG);
  PWC_Fcg1PeriphClockCmd(PWC_FCG1_PERIPH_USART3, Enable);
	
	stcUsartConf.enClkMode = UsartIntClkCkNoOutput;
	stcUsartConf.enDataLength = UsartDataBits8;
	stcUsartConf.enDirection = UsartDataLsbFirst;
	stcUsartConf.enParity = UsartParityNone;
	stcUsartConf.enStopBit = UsartOneStopBit;
	stcUsartConf.enSampleMode = UsartSamleBit8;
	
	Port_CFG.enPinMode = Pin_Mode_Out;
	PORT_Init(USART_TX_PORT, USART_TX_PIN, &Port_CFG);
	Port_CFG.enPinMode = Pin_Mode_In;
	PORT_Init(USART_RX_PORT, USART_RX_PIN, &Port_CFG);
	
	PORT_SetFunc(USART_RX_PORT, USART_RX_PIN, USART_RX_FUNC, Disable);
  PORT_SetFunc(USART_TX_PORT, USART_TX_PIN, USART_TX_FUNC, Disable);
	
	USART_UART_Init(USART_CH,&stcUsartConf); 
	USART_SetBaudrate(USART_CH, USART_BAUDRATE);

	stcIrqRegiCfg.enIRQn = Int005_IRQn;
	stcIrqRegiCfg.pfnCallback = UsartRxIrqCallback;
	stcIrqRegiCfg.enIntSrc = USART_RI_NUM;
	enIrqRegistration(&stcIrqRegiCfg);
	NVIC_SetPriority(stcIrqRegiCfg.enIRQn, DDL_IRQ_PRIORITY_00);
	NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
	NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);

	stcIrqRegiCfg.enIRQn = Int006_IRQn;
	stcIrqRegiCfg.pfnCallback = UsartErrIrqCallback;
	stcIrqRegiCfg.enIntSrc = USART_EI_NUM;
	enIrqRegistration(&stcIrqRegiCfg);
	NVIC_SetPriority(stcIrqRegiCfg.enIRQn, DDL_IRQ_PRIORITY_01);
	NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
	NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);	
	
	USART_FuncCmd(USART_CH, UsartRx, Enable);
  USART_FuncCmd(USART_CH, UsartRxInt, Enable);
	USART_FuncCmd(USART_CH, UsartTx, Enable);

	memset(hwdmx_rcvBuf,0,sizeof(hwdmx_rcvBuf));
	hwdmx_dataIsReady=FALSE;
	_receiveIndex=0;
}


void UsartRxIrqCallback(void)
{
  u2_t __Data;

	__Data = USART_RecData(USART_CH);
	
	if(hwdmx_dataIsReady == FALSE)
	{
		if(_receiveIndex>1)//通道1的值在第2个数据,从第2个byte开始接收
		{
			if(hwdmx_rcvBuf[1]==MSG_TYPE_CMD_FOR_ARM_SECOND_BYTE)
			{
				if(_receiveIndex>2)//通道1的值在第2个数据,从第2个byte开始接收
				{		
					if(_receiveIndex <= hwdmx_rcvBuf[2]+2)
					{
						hwdmx_rcvBuf[_receiveIndex]=__Data;//buf2开始接收	

						if (_receiveIndex  == hwdmx_rcvBuf[2]+2)
						{
							hwdmx_dataIsReady = TRUE;
							_receiveIndex=0;
							return;
						}	
					}									
				}
				else if(_receiveIndex==2)
				{
					hwdmx_rcvBuf[2] = __Data&0xff;				
				}
			}
			else if(hwdmx_rcvBuf[1]==MSG_TYPE_DMX_FOR_ARM_SECOND_BYTE)
			{			
				if((_receiveIndex > 1) 
				&&(_receiveIndex <= (1 + DMX_CMD_CHANNEL_NUM)))
				{
					hwdmx_rcvBuf[_receiveIndex]=__Data&0xff;//buf2开始接收	
					if (_receiveIndex  == 1+DMX_CMD_CHANNEL_NUM)
					{
						hwdmx_dataIsReady = TRUE;
						_receiveIndex=0;
					}	
				}								
			}
			_receiveIndex ++;		
		}
		else
		{
			if(((__Data&0xff)==MSG_TYPE_CMD_FOR_ARM_SECOND_BYTE)
			||((__Data&0xff)==MSG_TYPE_DMX_FOR_ARM_SECOND_BYTE))
			{
				hwdmx_rcvBuf[1] = __Data;	
				_receiveIndex=2;					 
			}
			else
			{
				_receiveIndex=1;
			}	
		}			
	}
}

void hwdmx_sendData(u1_t *data, u1_t len)
{
	u1_t __i;
	while(USART_CH->SR_f.TC == 0);
	for(__i=0;__i<len; __i++)
	{
		USART_SendData(USART_CH, data[__i]);
		while(USART_CH->SR_f.TC == 0);
	}
}


