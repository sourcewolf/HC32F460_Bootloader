#include "Hw_SPI1.h"
//#include "System_InterruptCFG_Def.h"
uint8_t SPI1_RX_Data;
bool flag_SPI3_RX, flag_SPI3_TX;
uint8_t RXbuff[SPI3_TRNCNT];
uint8_t TXbuff[SPI3_TRNCNT];
uint8_t DummyByte = 0x55;
void Prepare_data(void)
{
    uint32_t i;
    for(i=0;i<SPI3_TRNCNT;i++)
    {
        TXbuff[i] = i;
    }
}
void Hw_SPI1_TX_Callback(void)
{  
#ifdef SPI_SLAVE_MODE    
    M4_SPI3->DR = 0xFF;
#endif
}
void Hw_SPI1_RX_Callback(void)
{   
    //SPI_IrqCmd(SPI1_UNIT, SpiIrqReceive, Disable);//�ر��ж�
    DummyByte = SPI1_UNIT->DR;
}
void Hw_SPI1_IDEL_Callback(void)
{
    SPI_IrqCmd(SPI1_UNIT, SpiIrqIdel, Disable);
}
void Hw_SPI1_ERR_Callback(void)
{
    if(SPI1_UNIT->SR_f.MODFERF)//ģʽ����
    {
        SPI1_UNIT->SR_f.MODFERF = 0;
    }
    if(SPI1_UNIT->SR_f.UDRERF)//Ƿ�ش���
    {
        SPI1_UNIT->SR_f.UDRERF = 0;
    }
    if(SPI1_UNIT->SR_f.PERF)//��żУ�����
    {
        SPI1_UNIT->SR_f.PERF = 0;
    }
    if(SPI1_UNIT->SR_f.OVRERF)//���ش���
    {
        SPI1_UNIT->SR_f.OVRERF = 0;
    }
    SPI1_UNIT->CR1_f.SPE = 1;
}
void Hw_SPI1_Init(void)
{
    stc_spi_init_t stcSpiInit;
    stc_irq_regi_conf_t stcIrqRegiConf;

    /* configuration structure initialization */
    MEM_ZERO_STRUCT(stcSpiInit);
    MEM_ZERO_STRUCT(stcIrqRegiConf);
    Prepare_data();
    /* Configuration peripheral clock */
    PWC_Fcg1PeriphClockCmd(SPI1_UNIT_CLOCK, Enable);

    /* Configuration SPI pin */
    PORT_SetFunc(SPI1_SCK_PORT, SPI1_SCK_PIN, SPI1_SCK_FUNC, Disable);
    PORT_SetFunc(SPI1_NSS_PORT, SPI1_NSS_PIN, SPI1_NSS_FUNC, Disable);
    PORT_SetFunc(SPI1_MOSI_PORT, SPI1_MOSI_PIN, SPI1_MOSI_FUNC, Disable);
    PORT_SetFunc(SPI1_MISO_PORT, SPI1_MISO_PIN, SPI1_MISO_FUNC, Disable);

    /* Configuration SPI structure */
    stcSpiInit.enClkDiv = CLK_DIVISION;
    stcSpiInit.enFrameNumber = SpiFrameNumber1;
    stcSpiInit.enDataLength = SpiDataLengthBit8;
    stcSpiInit.enFirstBitPosition = SpiFirstBitPositionMSB;
    stcSpiInit.enSckPolarity = SpiSckIdleLevelLow;
    stcSpiInit.enSckPhase = SpiSckOddSampleEvenChange;
    stcSpiInit.enReadBufferObject = SpiReadReceiverBuffer;
    stcSpiInit.enWorkMode = SpiWorkMode4Line;
    stcSpiInit.enTransMode = SpiTransFullDuplex;
    stcSpiInit.enCommAutoSuspendEn = Disable;
    stcSpiInit.enModeFaultErrorDetectEn = Disable;
    stcSpiInit.enParitySelfDetectEn = Disable;
    stcSpiInit.enParityEn = Disable;
    stcSpiInit.enParity = SpiParityEven;

#ifdef SPI_MASTER_MODE
    stcSpiInit.enMasterSlaveMode = SpiModeMaster;
    stcSpiInit.stcDelayConfig.enSsSetupDelayOption = SpiSsSetupDelayCustomValue;
    stcSpiInit.stcDelayConfig.enSsSetupDelayTime = SpiSsSetupDelaySck1;
    stcSpiInit.stcDelayConfig.enSsHoldDelayOption = SpiSsHoldDelayCustomValue;
    stcSpiInit.stcDelayConfig.enSsHoldDelayTime = SpiSsHoldDelaySck1;
    stcSpiInit.stcDelayConfig.enSsIntervalTimeOption = SpiSsIntervalCustomValue;
    stcSpiInit.stcDelayConfig.enSsIntervalTime = SpiSsIntervalSck6PlusPck2;
    stcSpiInit.stcSsConfig.enSsValidBit = SpiSsValidChannel0;
    stcSpiInit.stcSsConfig.enSs0Polarity = SpiSsLowValid;
#endif

#ifdef SPI_SLAVE_MODE
    stcSpiInit.enMasterSlaveMode = SpiModeSlave;
    stcSpiInit.stcSsConfig.enSsValidBit = SpiSsValidChannel0;
    stcSpiInit.stcSsConfig.enSs0Polarity = SpiSsLowValid;
#endif
    SPI_Init(SPI1_UNIT, &stcSpiInit);
    
#ifdef SPI_SLAVE_MODE
    /* SPI3 tx interrupt */
    stcIrqRegiConf.enIntSrc = SPI1_TX_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI1_TX_IRQn;
    stcIrqRegiConf.pfnCallback = Hw_SPI1_TX_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);
    /* Enable software trigger interrupt */
//    enIntEnable(Int5);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
#endif
    /* SPI3 rx interrupt */
    stcIrqRegiConf.enIntSrc = SPI1_RX_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI1_RX_IRQn;
    stcIrqRegiConf.pfnCallback = Hw_SPI1_RX_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* SPI3 Error interrupt */
    stcIrqRegiConf.enIntSrc = SPI1_ERR_INT_SOURCE;
    stcIrqRegiConf.enIRQn = SPI1_ERR_IRQn;
    stcIrqRegiConf.pfnCallback = Hw_SPI1_ERR_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* SPI3 idel interrupt */
    stcIrqRegiConf.enIntSrc = SPI1_ERR_IDEL_SOURCE;
    stcIrqRegiConf.enIRQn = SPI1_IDEL_IRQn;
    stcIrqRegiConf.pfnCallback = Hw_SPI1_IDEL_Callback;
    /* Registration IRQ */
    enIrqRegistration(&stcIrqRegiConf);

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    
    /* Enable SPI */
    SPI_IrqCmd(SPI1_UNIT, SpiIrqReceive, Enable);
    SPI_IrqCmd(SPI1_UNIT, SpiIrqSend, Enable);
    SPI_IrqCmd(SPI1_UNIT, SpiIrqError, Enable);
    SPI_IrqCmd(SPI1_UNIT, SpiIrqIdel, Enable);
    SPI_Cmd(SPI1_UNIT, Enable);
    
}

void Hw_SPI1_TEST(void)
{
    while(SPI1_UNIT->SR_f.TDEF == 0);
    SPI_SendData8(SPI1_UNIT,0x55);	
}



