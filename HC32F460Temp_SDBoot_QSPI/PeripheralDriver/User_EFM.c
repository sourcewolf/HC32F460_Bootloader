
#include "User_EFM.h"




EEFLASH_data_t Buffer;
EEFLASH_data_t readdata;
static uint32_t CRdata;
//void User_EFM_Init(void)
//{
//    
//    EFM_Unlock();
//    EFM_FlashCmd(Enable);
//    while(Set != EFM_GetFlagStatus(EFM_FLAG_RDY));
//}

en_result_t FlashWritePage(uint32_t u32Addr, EEFLASH_data_t *eeflashdata)
{
    uint32_t i,pageaddr;
    pageaddr = u32Addr&0xFFFFE000;//取页起始地址，以保证数据从起始地址写
		
    printf("Prepare data:\r\n");  
    for(i = 0; i < (SECTORSIZE_W);i++)
    {
        eeflashdata->U32_Data[i] = i;
				readdata.U32_Data[i] = 0;//读数据数组清零
        printf("0x%x ",eeflashdata->U32_Data[i]);
    }
    printf("\r\n");
   /* Unlock EFM. */ 
    EFM_Unlock();
    
    /* Enable flash. */
    EFM_FlashCmd(Enable);
    /* Wait flash ready. */
    while(Set != EFM_GetFlagStatus(EFM_FLAG_RDY));
		
    EFM_SectorErase(pageaddr);//擦除页
		
    pageaddr = u32Addr;    
    for(i = 0; i < (SECTORSIZE_W);i++)
    {
        EFM_SingleProgram(pageaddr,eeflashdata->U32_Data[i]);
        pageaddr += 4;
    }		
    EFM_Lock();
		pageaddr = u32Addr;
    for(i = 0;i<(SECTORSIZE_W);i++)
    {
        readdata.U32_Data[i] = *(uint32_t*)pageaddr;
        pageaddr += 4;
    } 
    printf("Read Back data:\r\n");    
    for(i = 0; i < (SECTORSIZE_W);i++)
    {
        printf("0x%x ",readdata.U32_Data[i]);
    }
    printf("\r\n");
}

void User_CRC_Init(void)
{    
    PWC_Fcg0PeriphClockCmd(PWC_FCG0_PERIPH_CRC,Enable);
    M4_CRC->CR_f.SEL = 0;
    CRdata = M4_CRC->CR;
}

void User_CRC_Cal_16bit(uint8_t *data, uint8_t len,uint16_t *CRCdata)
{
    uint8_t i;
    CRC_Data_16bit_t Tempdata;
    for(i=0; i<len; i++)
    {
        Tempdata.Data_8bit[i] = data[i];
    }
    M4_CRC->RESLT = 0x00;//初始值
    for(i=0; i<(len/2); i++)
    {
        M4_CRC->DAT0 = Tempdata.Data_16bit[i];
    }
    *CRCdata = M4_CRC->RESLT;
}
void User_Test_CRC(void)
{
    uint8_t u8TestBuf[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    uint16_t CRC_result;
    User_CRC_Init();
    User_CRC_Cal_16bit(u8TestBuf,sizeof(u8TestBuf),&CRC_result);
    printf("CRC: %x\r\n",CRC_result);
}
en_result_t OTP_Write_Data(uint32_t u32Addr, uint32_t *data, uint8_t len)//写OTP区数据，注意：OTP同一地址只能写一次
{
    en_result_t restult = Ok;
    if(data == NULL)
    {
        return ErrorInvalidParameter;
    }
    if(len>OTP_SECTORSIZE_W)
    {
        return ErrorInvalidParameter;
    }
    uint32_t i,pageaddr;
    pageaddr = u32Addr;
    EFM_Unlock();
    /* Enable flash. */
    EFM_FlashCmd(Enable);
    /* Wait flash ready. */
    while(Set != EFM_GetFlagStatus(EFM_FLAG_RDY));
    EFM_SectorErase(pageaddr);
    for(i = 0;i<len;i++)
    {
        restult = EFM_SingleProgram(pageaddr,data[i]);
        pageaddr += 4;
    } 
    EFM_Lock();
    return Ok;
}
en_result_t OTP_Verify_Data(uint32_t u32Addr, uint32_t *data, uint8_t len)
{
    int result;
    uint32_t *pdata;
    pdata = (uint32_t *)u32Addr;
    result = memcmp(pdata,data,len);
    if(result == 0)
    {
        return Ok;
    }
    return Error;
}
void testOTP(void)
{
    uint8_t i;
    OTPData_t otpdata;
    en_result_t result;
    for(i=0;i<OTP_SECTORSIZE;i++)
    {
        otpdata.U8_data[i] = 0xFF;
    }
    OTP_Write_Data(0x03000C00,otpdata.U32_data,OTP_SECTORSIZE_W);
    result = OTP_Verify_Data(0x03000C00,otpdata.U32_data,OTP_SECTORSIZE_W);
    printf("OTP Test Result %d",result);
}