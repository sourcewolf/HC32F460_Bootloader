#ifndef _HD_PRESS_AND_BAT_H_
#define _HD_PRESS_AND_BAT_H_

#include "hc32_ddl.h"

extern volatile uint16_t _adc_value_press;
extern volatile bool b_press_switch_on;

//ADC硬件资源初始化，用于采集传感器力敏电阻压力值
void hd_press_and_bat_adc_hw_init();

//测试press and bat adc hardware
void test_hd_press_and_bat_ad_hw_task();

void ADC2ChCmp_IrqHandler(void);

/* Timer definition for this example. */
#define TMR_UNIT                    M4_TMR02
#define ENABLE_TMR0()               PWC_Fcg2PeriphClockCmd(PWC_FCG2_PERIPH_TIM02, Enable)

static void TimerConfig(void);

static void AdcTriggerConfig(void);

//摄像头定义为三个状态，摄像头打开状态、摄像头关闭状态、摄像头待关闭状态
typedef enum _E_CAMERA_STA{
    E_CAMERA_STA_POWEON = 0,
    E_CAMERA_STA_WAIT_POWEROFF,
    E_CAMERA_STA_POWEROFF,
    E_CAMERA_STA_SEND_STROKE_TAIL
}E_CAMERA_STA;

#endif
