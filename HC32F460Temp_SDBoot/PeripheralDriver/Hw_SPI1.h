#ifndef HW_SPI1_H
#define HW_SPI1_H
#include "hc32_ddl.h"
//unsigned char SPI_DATA;
/* Choose SPI master or slave mode */
#define SPI_MASTER_MODE
//#define SPI_SLAVE_MODE
/* SPI_SCK Port/Pin definition */
#ifdef SPI_MASTER_MODE
#define CLK_DIVISION        SpiClkDiv64
#define SPI3_TRNCNT              (50u)//传输次数
#endif
#ifdef SPI_SLAVE_MODE
#define CLK_DIVISION        SpiClkDiv2
#define SPI_DMA_TRNCNT              (40*1024)//传输次数
#endif
#define SPI1_SCK_PORT                    PortA
#define SPI1_SCK_PIN                     Pin00
#define SPI1_SCK_FUNC                    Func_Spi1_Sck

/* SPI_NSS Port/Pin definition */
#define SPI1_NSS_PORT                    PortA
#define SPI1_NSS_PIN                     Pin01
#define SPI1_NSS_FUNC                    Func_Spi1_Nss0

/* SPI_MOSI Port/Pin definition */
#define SPI1_MOSI_PORT                   PortA
#define SPI1_MOSI_PIN                    Pin02
#define SPI1_MOSI_FUNC                   Func_Spi1_Mosi

/* SPI_MISO Port/Pin definition */
#define SPI1_MISO_PORT                   PortA
#define SPI1_MISO_PIN                    Pin03
#define SPI1_MISO_FUNC                   Func_Spi1_Miso

/* SPI unit and clock definition */
#define SPI1_UNIT                        M4_SPI1
#define SPI1_UNIT_CLOCK                  PWC_FCG1_PERIPH_SPI1
#define SPI1_TX_INT_SOURCE               INT_SPI1_SRTI
#define SPI1_RX_INT_SOURCE               INT_SPI1_SRRI
#define SPI1_ERR_INT_SOURCE              INT_SPI1_SPEI
#define SPI1_ERR_IDEL_SOURCE             INT_SPI1_SPII

#define SPI1_TX_IRQn            Int004_IRQn
#define SPI1_RX_IRQn            Int005_IRQn
#define SPI1_ERR_IRQn           Int006_IRQn
#define SPI1_IDEL_IRQn          Int007_IRQn

void Hw_SPI1_Init(void);
void Hw_SPI1_TEST(void);

#endif
