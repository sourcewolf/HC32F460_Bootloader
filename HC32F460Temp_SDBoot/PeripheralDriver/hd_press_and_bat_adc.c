#include "hd_press_and_bat_adc.h"
#include "hc32_ddl.h"

#include "msg_dbg.h"
#include "cmsis_os.h"
#include "asl_time.h"
#include "bsm_dev_info.h"
#include "bsm_sensor_control.h"
#include <stdbool.h>
#include "bsm_battery_detect.h"
#include "bsm_fpga_control.h"
#include "hd_power_gpio.h"
#include "asl_os_semaphore.h"
#include "bsm_dec_service.h"
#include "bsm_spi_recv_pingpang_buff.h"
#include "bsm_shared_memory.h"
#include "bsm_bt_debug_buf_send.h"
#include "routine_handle_event.h"
#include "hd_power_gpio.h"
#include "hc32f46x_adc.h"

static TaskHandle_t _hd_task_adc_press_and_bat;

//adc模块，压力电阻 与 电池电压 采样值
//压力电阻 ADC1_IN1, PA1, 电池电压 ADC1_IN0, PA0
//注：此压力电阻值需要被每帧解码时引用，以表明每个坐标对应的压力值，精度上向右滞后大概20ms左右
volatile uint16_t _adc_value_press;
volatile bool b_press_switch_on;

static uint16_t _adc_value_bat;

//定义由WAIT POWEROFF 到 POWEROFF 状态切换的最小时间间隔ticks值
#define MIN_TIME_INTERVAL_TICKS_WAIT_POWEROFF_TO_POWEROFF     1//单位为两个ADC检测周期，即1*6 = 12ms
static volatile unsigned int _wait_poweroff_to_poweroff_ticks_count = 0;

//定义抬笔手，隔多少时间，再发送笔画尾事件
#define MIN_TIME_SEND_STROKE_TAIL_TIME     2//单位为两个ADC检测周期，即2*6 = 12ms


volatile E_CAMERA_STA _b_sensor_power_on = E_CAMERA_STA_SEND_STROKE_TAIL;

//volatile bool b_reload_flag = true;
//注：由于FPGA复位后，会截断正常传输的SPI数据，此接口提供关SENSOR时，重新设置DMA参数
extern void reset_spi_dma_parameter();

//static void get_adc_data(uint16_t *_adc_value_press, uint16_t *_adc_value_bat){
//    *_adc_value_press = M4_ADC1->DR1;
//    *_adc_value_bat = M4_ADC1->DR0;
//}

//MCU总共是17个通道
#define ADC1_CHANNEL_COUNT          (17u)
static uint16_t m_au16Adc1Value[ADC1_CHANNEL_COUNT];

static void adc_callback(){
//    get_adc_data(&_adc_value_press, &_adc_value_bat);
//	if (b_reload_flag)
//	{
//		b_reload_flag = false;
//	}
//	else
//	{
//		DEBUGE_ISR_ENV("!!!!!!!!!!!!!!!reload\r\n");
//	}

    //如果ADC还在转换待机中，则直接退出，不取ADC的值
    if (Set == ADC_GetEocFlag(M4_ADC1, ADC_SEQ_A)){
        ADC_GetChData(M4_ADC1, (ADC1_CH0 | ADC1_CH1), m_au16Adc1Value, 2);
        _adc_value_press = m_au16Adc1Value[1];
        _adc_value_bat   = m_au16Adc1Value[0];
        /* Clear EOC flag. */
        ADC_ClrEocFlag(M4_ADC1, ADC_SEQ_A);
    }else{
        return;
    }

//    _adc_value_press = M4_ADC1->DR1;
//    _adc_value_bat   = M4_ADC1->DR0;


    //for debug, remove me
//    _adc_value_press = 2000;

#if EBABLE_IMAGE_BYPASS_TO_PC
    //查看图像模式下，不需要再响应压力传感器的开关
    return;
#endif

    //6ms一次检测间隔，每8次检测一次，相当于48ms一次，采样21次计算一次电压值 48*21=981ms
	//注；低电检测事件一秒一次，检测电压时间不应该超过1秒
    static unsigned int bat_adc_sample_interval = 0;
    //注：用于统计开机前的16秒时间，特殊处理计算电池电压值
    static unsigned int bat_det_time_count = 0;

    //_bsm_bat.bat_volt_level = _adc_value_bat * 1.0 / 4096.0 * 3.3 * 156.0 / 56.0 / 2.0;
    //if (_b_sensor_power_on == E_CAMERA_STA_SEND_STROKE_TAIL)
    {
        //6ms一次检测间隔，每20次检测一次，相当于120ms一次，1秒采集8次
        if (bat_adc_sample_interval++ % (20) == 0) {
            if (_bsm_bat.cur_store_index == MAX_BAT_SAMPLE_TICKS) {
                //触发一次BAT值计算，以正常模式调用
                bsm_battery_detect_calculate(false);

                //清零，重新放入采样值
                _bsm_bat.cur_store_index = 0;
                _bsm_bat.bat_sample[_bsm_bat.cur_store_index++] = _adc_value_bat;
            }
            else {
                _bsm_bat.bat_sample[_bsm_bat.cur_store_index++] = _adc_value_bat;
                if (_routine_info.b_send_dgb_data &&_routine_info.dbg_type == E_PRESS_VAL)
                {
                    dbgmode_collect_pressure(_adc_value_press, _bsm_bat.bat_sample[_bsm_bat.cur_store_index - 1], b_press_switch_is_on());
                }
            }
        }

        //开机前16秒，每次直接用已经缓存好的采样值计算电池电压值
        //注：计算间隔时间为1秒计算一次
        bat_det_time_count ++;
        if(6*bat_det_time_count > 1000 &&
                6*bat_det_time_count < (TIME_BAT_SLIDE_S*1000) &&
                bat_det_time_count % 166 == 0){
            //以开机模式计算调用
            bsm_battery_detect_calculate(true);
        }
    }

	//注：由于笔帽干涉笔芯，造成套上笔帽随机出现，充电线接上，但指示灯没有切换过来，在此处，如果发现笔帽已经套上，则不打开摄像头
	//注：硬件原因，holl在充电线插入
//    bool holl_sw = hd_b_hall_switch_nearly_magnet();
//    if(holl_sw){
	//检测到充电线情况下，不开摄像头
	//保留电压检测
	bool usb_charge_insert = hd_b_charge_line_insert();
	if (usb_charge_insert) {
		//检测到笔帽套上后，不再开启摄像头，关机操作还是给到timer中去处理
		bsm_fpga_state_switch(FPGA_IDLE_MODE);
		enable_sensor_work_state(false);
		goto normal_exit;
	}

	if (_routine_info.b_send_dgb_data && _routine_info.dbg_type == E_GREY_IMAGE)
	{
		goto normal_exit;
	}

    //摄像头开启，如果摄像头已经处于工作状态，则不需要执行下面的逻辑，避免重复操作
    if((_b_sensor_power_on == E_CAMERA_STA_SEND_STROKE_TAIL || _b_sensor_power_on == E_CAMERA_STA_POWEROFF) &&
            (_adc_value_press > _pen_dev_info.pressure_value /*| b_press_switch_on*/) &&
		//低电量或OTA升级不响应按压操作
        (_routine_info.bat_low_level_tick_count < LOWER_BAT_DET_SHAKE_VALUE)&&(!_routine_info.b_ota_upgrade))
	{
        DEBUGN_ISR_ENV("camera status change :  E_CAMERA_STA_SEND_STROKE_TAIL -> E_CAMERA_STA_POWEON, _pen_dev_info.pressure_value = %d, _adc_value_press = %d \r\n"
                       ,_pen_dev_info.pressure_value,_adc_value_press);

        //注：此处将PING PANG buff全部清除掉，防止笔画开头事件，第一个点用了此缓存数据
        memset(&_bsm_shared_mem.MEM_DEC._spi_recv_pingpang_buff[0][0], 0, MAX_RECV_BUFF_SIZE);
        memset(&_bsm_shared_mem.MEM_DEC._spi_recv_pingpang_buff[1][0], 0, MAX_RECV_BUFF_SIZE);

        //注：防止从idle切换到工作状态时，先设置FPGA,再打开摄像头，确保切换到工作状态后，图像帧数据不出现异常帧
#if EBABLE_IMAGE_BYPASS_TO_PC != 1
		enable_sensor_work_state(true);
		bsm_fpga_state_switch(FPGA_NORMAL_MODE_OUTPUT_COORD);
//        asl_time_delayms_nonos(2);
#endif

        //每次新的笔画时，此记数器清0，确定前面几帧数据不抽掉
        extern volatile unsigned int dbg_send_dot_data_to_pc_interval_count;
        dbg_send_dot_data_to_pc_interval_count = 0;

        //打开摄像头
        _b_sensor_power_on = E_CAMERA_STA_POWEON;
        //todo: 填一个笔画开头包，发送到笔画队列中
        goto normal_exit;
    }

    //如果摄像头已经处于打开状态，则关闭摄像头（之前没有打开操作，则即使采样值低于阈值，也不执行下面逻辑，因为可能是正常的没有按压笔芯的动作）
    if(_b_sensor_power_on == E_CAMERA_STA_POWEON &&
            (_adc_value_press < _pen_dev_info.pressure_value /*| !b_press_switch_on*/)){
        DEBUGN_ISR_ENV("camera status change :  E_CAMERA_STA_POWEON -> E_CAMERA_STA_WAIT_POWEROFF \r\n");

        _b_sensor_power_on = E_CAMERA_STA_WAIT_POWEROFF;

        //开启wait poweroff to poweroff的时间记时
        _wait_poweroff_to_poweroff_ticks_count = 0;

        //todo: 填一个笔画结尾包，发送到笔画队列中
        //注：在此处发送结尾包，解码模块端，并不能马上认定为笔画结束，因为此时刻，FPGA接收图像并解码存在一定的几ms延时
        //结论：1、直接让解码模块，以收到笔画开头包事件后，超时接收队列，如果超过多少ms没有数据解码，则认定为笔画结束，发送笔画结尾包
        //     2、如果超时时间内收到笔画开头包，则认定前一笔画已经结束，需要先发送笔画结尾包，再发送新的笔画开头包
        goto normal_exit;
    }

    if(_b_sensor_power_on == E_CAMERA_STA_WAIT_POWEROFF &&
            (_adc_value_press > _pen_dev_info.pressure_value /*| b_press_switch_on*/) &&
        (_routine_info.bat_low_level_tick_count < LOWER_BAT_DET_SHAKE_VALUE)){

        DEBUGN_ISR_ENV("camera status change :  E_CAMERA_STA_WAIT_POWEROFF -> E_CAMERA_STA_POWEON \r\n");

        _b_sensor_power_on = E_CAMERA_STA_POWEON;
        goto normal_exit;
    }

    if(_b_sensor_power_on == E_CAMERA_STA_WAIT_POWEROFF &&
            (_adc_value_press < _pen_dev_info.pressure_value /*| !b_press_switch_on*/) &&
            _wait_poweroff_to_poweroff_ticks_count >= MIN_TIME_INTERVAL_TICKS_WAIT_POWEROFF_TO_POWEROFF){

        DEBUGN_ISR_ENV("camera status change :  E_CAMERA_STA_WAIT_POWEROFF -> E_CAMERA_STA_POWEROFF \r\n");

        //注：考虑到关摄像头后，FPGA通过SPI输出数据，会有一定的延时，所有在此处进行延时逻辑设计，等待3个时间周期后发送笔抬起信息
#if EBABLE_IMAGE_BYPASS_TO_PC != 1
        //todo : 关闭摄像头，摄像头可以第一时间关闭，但是笔抬起状态可以稍缓
        bsm_fpga_state_switch(FPGA_IDLE_MODE);
        enable_sensor_work_state(false);
#endif

        DEBUGN_ISR_ENV("NOT BLOCK \r\n");

        _b_sensor_power_on = E_CAMERA_STA_POWEROFF;
        goto normal_exit;
    }

    if(_b_sensor_power_on == E_CAMERA_STA_POWEROFF &&
            (_adc_value_press < _pen_dev_info.pressure_value /*| !b_press_switch_on*/) &&
            _wait_poweroff_to_poweroff_ticks_count >= MIN_TIME_INTERVAL_TICKS_WAIT_POWEROFF_TO_POWEROFF){

        _b_sensor_power_on = E_CAMERA_STA_SEND_STROKE_TAIL;

        //BUG: 造成整个中断被阻塞在此处？
        //BUG: 太高中断优先级，造成freeRtos 的api不能在中断中调用
        extern asl_os_semaphore _semaphore_recv_new_frame;
        asl_os_release_semaphore(_semaphore_recv_new_frame);

        goto normal_exit;
    }

    _wait_poweroff_to_poweroff_ticks_count ++;

normal_exit:
	//1s 左右采样一次
	
    static unsigned int dbg_count = 0;
    if (dbg_count++ % (166/4) == 0) {
        b_press_switch_on = b_press_switch_is_on();
        DEBUG_ADC("_adc_value_bat = %d, _adc_value_press = %d, _b_sensor_power_on = %d, b_press_switch_on = %d, _bsm_bat.bat_volt_level = %f\r\n",
            _adc_value_bat, _adc_value_press, _b_sensor_power_on, b_press_switch_on, _bsm_bat.bat_volt_level);
    }
//	b_reload_flag = true;
}

/**
 *******************************************************************************
 ** \brief  Set an ADC pin as analog input mode or digit mode.
 **
 ******************************************************************************/
static void AdcSetPinMode(uint8_t u8AdcPin, en_pin_mode_t enMode)
{
    en_port_t enPort = PortA;
    en_pin_t enPin   = Pin00;
    bool bFlag       = true;
    stc_port_init_t stcPortInit;

    MEM_ZERO_STRUCT(stcPortInit);
    stcPortInit.enPinMode = enMode;
    stcPortInit.enPullUp  = Disable;

    switch (u8AdcPin)
    {
    case ADC1_IN0:
        enPort = PortA;
        enPin  = Pin00;
        break;

    case ADC1_IN1:
        enPort = PortA;
        enPin  = Pin01;
        break;

    case ADC1_IN2:
        enPort = PortA;
        enPin  = Pin02;
        break;

    case ADC1_IN3:
        enPort = PortA;
        enPin  = Pin03;
        break;

    case ADC12_IN4:
        enPort = PortA;
        enPin  = Pin04;
        break;

    case ADC12_IN5:
        enPort = PortA;
        enPin  = Pin05;
        break;

    case ADC12_IN6:
        enPort = PortA;
        enPin  = Pin06;
        break;

    case ADC12_IN7:
        enPort = PortA;
        enPin  = Pin07;
        break;

    case ADC12_IN8:
        enPort = PortB;
        enPin  = Pin00;
        break;

    case ADC12_IN9:
        enPort = PortB;
        enPin  = Pin01;
        break;

    case ADC12_IN10:
        enPort = PortC;
        enPin  = Pin00;
        break;

    case ADC12_IN11:
        enPort = PortC;
        enPin  = Pin01;
        break;

    case ADC1_IN12:
        enPort = PortC;
        enPin  = Pin02;
        break;

    case ADC1_IN13:
        enPort = PortC;
        enPin  = Pin03;
        break;

    case ADC1_IN14:
        enPort = PortC;
        enPin  = Pin04;
        break;

    case ADC1_IN15:
        enPort = PortC;
        enPin  = Pin05;
        break;

    default:
        bFlag = false;
        break;
    }

    if (true == bFlag)
    {
        PORT_Init(enPort, enPin, &stcPortInit);
    }
}


/**
 *******************************************************************************
 ** \brief  Config the pin which is mapping the channel to analog or digit mode.
 **
 ******************************************************************************/
static void AdcSetChannelPinMode(M4_ADC_TypeDef *ADCx,
                                 uint32_t u32Channel,
                                 en_pin_mode_t enMode)
{
    uint8_t u8ChIndex;
#if (ADC_CH_REMAP)
    uint8_t u8AdcPin;
#else
    uint8_t u8ChOffset = 0u;
#endif

    u32Channel &= ADC1_PIN_MASK_ALL;
    u8ChIndex   = 0u;
    while (0u != u32Channel)
    {
        if (u32Channel & 0x1ul)
        {
#if (ADC_CH_REMAP)
            u8AdcPin = ADC_GetChannelPinNum(ADCx, u8ChIndex);
            AdcSetPinMode(u8AdcPin, enMode);
#else
            AdcSetPinMode((u8ChIndex + u8ChOffset), enMode);
#endif
        }

        u32Channel >>= 1u;
        u8ChIndex++;
    }
}

static void start_adc_convert(void)
{
    M4_ADC1->STR = 1;
}

//采用timer 0 事件触发ADC，作为ADC采样触发源，2ms左右触发一次，
//在ADC中断处理函数中，对采样回来力敏电阻值，和电池电压值作简单处理马上返回
void hd_press_and_bat_adc_hw_init(void)
{
    //这个值最大可设置为255,这样提高采样时间，可以降低影响
    uint8_t au8Adc1SaSampTime[4] = { 0xFF,     0xFF,     0xFF,     0xFF };

	stc_adc_init_t stcAdcInit;
    stc_adc_ch_cfg_t  stcAdcBaseCFG;
    stc_irq_regi_conf_t stcIrqRegiConf;
    stc_port_init_t Port_CFG;
    
    MEM_ZERO_STRUCT(stcAdcInit);
    MEM_ZERO_STRUCT(stcAdcBaseCFG);
    MEM_ZERO_STRUCT(stcIrqRegiConf);
    MEM_ZERO_STRUCT(Port_CFG);
    
     /* 1. Enable ADC1. */
    PWC_Fcg3PeriphClockCmd(PWC_FCG3_PERIPH_ADC1, Enable);
    //ADC时钟为 168/4 = 42MHZ
    stcAdcInit.enScanMode = AdcMode_SAOnce;
    stcAdcInit.enDataAlign = AdcDataAlign_Right;
    stcAdcInit.enResolution = AdcResolution_12Bit;
    stcAdcInit.enAutoClear = AdcClren_Disable;
//    stcAdcInit.enAverageCount = AdcAvcnt_4;
    ADC_Init(M4_ADC1, &stcAdcInit);

    Port_CFG.enPinMode = Pin_Mode_Ana;
    PORT_Init(PortA, Pin01, &Port_CFG);
    
    //配置 press adc channel
    /* 1. Set the ADC pin to analog mode. */
    AdcSetChannelPinMode(M4_ADC1, ADC1_CH0, Pin_Mode_Ana);
    AdcSetChannelPinMode(M4_ADC1, ADC1_CH1, Pin_Mode_Ana);

    /* 2. Add ADC channel. */
    stcAdcBaseCFG.u32Channel = ADC1_CH1 | ADC1_CH0;
//    stcAdcBaseCFG.enAvgEnable = true;
    stcAdcBaseCFG.pu8SampTime = au8Adc1SaSampTime;
    stcAdcBaseCFG.u8Sequence = ADC_SEQ_A;//Must be setting, Default can nog convert data
    ADC_AddAdcChannel(M4_ADC1, &stcAdcBaseCFG);

     /* 3. Config PGA and add PGA channels. */
    ADC_ConfigPga((en_adc_pga_factor_t)AdcPgaFactor_2, AdcPgaNegative_VSSA);
    /* Add PGA pin ADC1_IN0, ADC1_IN2, ADC12_IN5 */
    ADC_AddPgaChannel(ADC1_CH0);
    ADC_PgaCmd(Enable);

    //配置ADC的触发源
    AdcTriggerConfig();

    _b_sensor_power_on = E_CAMERA_STA_POWEROFF;

    //配置 中断
    stcIrqRegiConf.enIntSrc = INT_ADC1_EOCA;
    stcIrqRegiConf.pfnCallback = adc_callback;
	stcIrqRegiConf.enIRQn = ADC1_EOCA_IRQn;
	enIrqRegistration(&stcIrqRegiConf);

    //注：ADC检测的中断优先级 比UART BT RX中断略低，保证数据接收不上溢
    //BUG: 太高中断优先级，造成freeRtos 的api不能在中断中调用
//    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_01);
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);

    M4_ADC1->ISR_f.EOCAF  = 0;
    M4_ADC1->ICR_f.EOCAIEN = 1;
    
    //是否使能中断功能
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);//Enable Interrupt

    //开启timer0 产生ADC触发源事件
    TimerConfig();
}

static void test_adc_task(void *param){
    while(1){
        start_adc_convert();
        
        get_adc_data(&_adc_value_press, &_adc_value_bat);
        DEBUGN("_adc_value_press = %d,  _adc_value_bat = %d\r\n", _adc_value_press, _adc_value_bat);

        //测试时间获取接口
        DEBUGN("get system cur ticks : %d \r\n", asl_time_get_systickms());
        asl_time_delayms(1000);
        DEBUGN("get system cur ticks : %d \r\n", asl_time_get_systickms());
        asl_time_delayms(1000);
    }
}


void test_hd_press_and_bat_ad_hw_task()
{
    DEBUGN("enter \r\n");

    hd_press_and_bat_adc_hw_init();

//    start_adc_convert();
    
    //创建测试task，测试SPI的收发数据
    xTaskCreate(test_adc_task,(const char *)"dc", configMINIMAL_STACK_SIZE,
                NULL, tskIDLE_PRIORITY+3, &_hd_task_adc_press_and_bat);

    DEBUGN("exit \r\n");
}

//void ADC2ChCmp_IrqHandler(void)
//{
//    uint32_t u32AwdOkCh;

//    u32AwdOkCh = ADC_GetAwdFlag(M4_ADC2);

//    if (0u != u32AwdOkCh)
//    {
//        m_u32AdcIrqFlag |= ADC2_CHCMP_IRQ_BIT;
//    }

//    if (ADC2_AWD_CH0 & u32AwdOkCh)
//    {
//        ADC_ClrAwdChFlag(M4_ADC2, ADC2_AWD_CH0);
//        // TODO: YOUR CODE
//    }

//    if (ADC2_AWD_CH1 & u32AwdOkCh)
//    {
//        ADC_ClrAwdChFlag(M4_ADC2, ADC2_AWD_CH1);
//        // TODO: YOUR CODE
//    }

//    if (ADC2_AWD_CH2 & u32AwdOkCh)
//    {
//        ADC_ClrAwdChFlag(M4_ADC2, ADC2_AWD_CH2);
//        // TODO: YOUR CODE
//    }
//}

/**
 *******************************************************************************
 ** \brief  Timer configuration, for generating event EVT_TMR02_GCMA every second.
 **
 ** \param  None.
 **
 ** \retval None.
 **
 ******************************************************************************/
static void TimerConfig(void)
{
    stc_tim0_base_init_t stcTimerCfg;
    stc_clk_freq_t stcClkTmp;
    uint32_t u32Pclk1;

    MEM_ZERO_STRUCT(stcTimerCfg);

//    /* The new system clock frequency is 168MHz, PCLK1 is 84MHz by default. */
//    m_stcSysclkCfg.enPclk1Div = ClkSysclkDiv16;
//    CLK_SysClkConfig(&m_stcSysclkCfg);

    /* Get PCLK1. */
    CLK_GetClockFreq(&stcClkTmp);
    u32Pclk1 = stcClkTmp.pclk1Freq;

    /* Timer0 peripheral enable. */
    ENABLE_TMR0();
    /* Config register for channel A. */
    stcTimerCfg.Tim0_CounterMode = Tim0_Sync;
    stcTimerCfg.Tim0_SyncClockSource = Tim0_Pclk1;

    //84MHZ / 1024 = 0.08203125 MHZ = 82 KHZ  ~~ 1ms的采样间隔
    // 1024 / 84 * 80 = 975us
    stcTimerCfg.Tim0_ClockDivision = Tim0_ClkDiv1024;
    /* Tim0_CmpValue's type is uint16_t!!! Be careful!!! */
    // 1024 / 84 * 492 = 5997us = 6ms
    stcTimerCfg.Tim0_CmpValue = 492;//80;
    TIMER0_BaseInit(TMR_UNIT, Tim0_ChannelA, &stcTimerCfg);

    /* Start timer0. */
    TIMER0_Cmd(TMR_UNIT, Tim0_ChannelA, Enable);
}

/**
 *******************************************************************************
 ** \brief  Set ADC1 sequence A and ADC2 sequence A trigger source.
 **
 ******************************************************************************/
static void AdcTriggerConfig(void)
{
    stc_adc_trg_cfg_t stcTrgCfg;

    MEM_ZERO_STRUCT(stcTrgCfg);

#if 1//(ADC1_TRIGGER_MODE == INTERNAL_EVENT_TRIGGER)
    /*
     * If select an event(@ref en_event_src_t) to trigger ADC,
     * AOS must be enabled first.
     */
    /* The AOS function is used in this example. */
    #define ENABLE_AOS()                PWC_Fcg0PeriphClockCmd(PWC_FCG0_PERIPH_PTDIS, Enable)
    ENABLE_AOS();

    /* Select EVT_TMR02_GCMA as ADC1 sequence A trigger source. */
    stcTrgCfg.u8Sequence = ADC_SEQ_A;
    stcTrgCfg.enTrgSel   = AdcTrgsel_TRGX0;
    stcTrgCfg.enInTrg0   = EVT_TMR02_GCMA;
    ADC_ConfigTriggerSrc(M4_ADC1, &stcTrgCfg);
    ADC_TriggerSrcCmd(M4_ADC1, ADC_SEQ_A, Enable);
#endif

//#if (ADC2_TRIGGER_MODE == EXTERNAL_PIN_TRIGGER)
//    /*
//     *  ADC2:
//     *  Enable sequence A external trigger source.
//     *  Set ADTRGX(X = 2; ADTRG1 = PB6) as the trigger source.
//     *  ADTRGX from high to low and stays low 1.5 * PCLK4 cycles
//     *  or more will trigger ADC conversion.
//     */
//    stcTrgCfg.u8Sequence = AdcSequence_A;
//    stcTrgCfg.enTrgSel   = AdcTrgsel_ADTRGX;

//    PORT_SetFunc(PortB, Pin06, Func_Adtrg, Enable);
//    ADC_ConfigTriggerSrc(M4_ADC2, &stcTrgCfg);
//    ADC_TriggerSrcCmd(M4_ADC2, AdcSequence_A, Enable);
//#endif
}
